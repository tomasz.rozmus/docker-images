# Docker Images

A place for my docker images. 

## Image types documentation:
* [Builders](builders)
* [Servers](servers)

## Modifying image

Build with:  
```docker build -f <path_to>/Dockerfile -t xnyl/<type>-<subtype>:<version> [./<path_to>/<content>]```  

Login to docker:
```docker login docker.io```    

Push image
```docker push <image>```  

Test image

```docker run -it <image> bash```


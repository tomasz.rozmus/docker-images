FROM node:10

# install typescript, tsnode and lerna -- should not chage often
RUN npm install -g typescript ts-node lerna

# install gitlab-runner and docker@18
RUN curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 && \
    chmod +x /usr/local/bin/gitlab-runner && \
    export VERSION=18.09.7 && curl -sSL https://get.docker.com/ | sh && \
    useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Note: Latest version of kubectl may be found at:
# https://github.com/kubernetes/kubernetes/releases
ENV KUBE_LATEST_VERSION="v1.16.2"
# Note: Latest version of helm may be found at:
# https://github.com/kubernetes/helm/releases
ENV HELM_VERSION="v2.15.1"

RUN apt-get install openssh-client --yes \
    && wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && wget -q https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm

# install posgres client (for stretch debian version)
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
    && echo  "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
    && apt-get update \
    && apt-get install postgresql-client-11 --yes

# Install latest chrome dev package, which installs the necessary libs to
     # make the bundled version of Chromium that Puppeteer installs work.
RUN  wget --quiet -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
     && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
     && apt-get update \
     && apt-get install -y google-chrome-unstable --no-install-recommends \
     && rm -rf /var/lib/apt/lists/* \
     && wget --quiet https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -O /usr/sbin/wait-for-it.sh \
     && chmod +x /usr/sbin/wait-for-it.sh

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

# Puppeteer v1.19.0 works with Chromium 77.
RUN yarn global add puppeteer@1.20.0

# register runner
RUN gitlab-runner register \
        	-u https://gitlab.com \
        	-r qjwiuBZTwmpXnvB6gKoa \
        	-n \
        	--executor docker \
        	--tag-list "typescript"\
        	--name "TypeScript Runner" \
        	--docker-image "docker:18.09.7" \
            --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
            --docker-privileged \
            --cache-dir "cache" && \
    gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner && \
    gitlab-runner start

# Servers

## Nginx
Nginx image for web applications. 

### Dynamic configuration
It generates dynamically `env.config.js` file from local `.env` file during startup.
With appropriate caching for `<script src="/env.config.js"></script>`, it enables web apps to be dynamically configured, without the need of rebuilding or image recreation.

Given `.env` file contains: 
```dotenv
API_BASE_URL=https://example.com/api
SOME_API_KEY=
```
and environment variable `SOME_API_KEY=0x0abc123`,
it generates `env.config.js` with:
```js
window._env_ = {
API_BASE_URL: "https://example.com/api",
SOME_API_KEY: "0x0abc123"
}

```

### Misc
Edit config:
```
vi /etc/nginx/conf.d/default.conf
```
Reload:
```
nginx -s reload
```

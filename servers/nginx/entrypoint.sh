#!/bin/sh

# create dynamic js-config //TODO: investigate why .env is not present
#sh config_creator.sh
#mv env-config.js static/js/env-config.js
rm config_creator.sh

# echo AuthType Basic htpasswd file
echo $HTPASSWD_FILE > /etc/nginx/conf.d/htpasswd

# This will exec the CMD from your Dockerfile
exec "$@"
